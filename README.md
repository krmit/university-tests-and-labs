# Exempel på tentor och labbar

Detta är en samling av tetor och laborationer ifrån olika utbildnignar i 
den svenska högskolevärlden. Observerat att dessa är utlämnade uppgifter
ifrån universteten och är därför offentligahandlingar. Copyrighten av 
dessa tentor och prov tillhör dock den lärare som har gjort dem.

Därför delas dessa prov och labbar bara i en sluten gruppen som är 
intressserade av att jämföra denna typ av uppgifter. Om du vill 
återpublisera dessa uppgifter i någon from måste du kontakta denna lärare
som skapat dem.
